﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rendezes
{
    /// <summary>
    /// Fejlesztő2:Készítse el a gyorsendezést megvalósító algoritmus implementációját és tegye lehetővé a végrehajtás idejének mérését. 
    /// Használja a beépített lista generic típust
    /// </summary>
    static class GyorsRendezes
    { 
        public static void Futtat(List<int> szamok)
        { 
            Console.WriteLine("Rendezés előtt:");
            ListaKiir(szamok);
            Console.WriteLine("Minimum rendezés:");                 
            DateTime startTime = DateTime.Now;
            //Console.WriteLine("startTime = {0}", startTime);
            Gyorsrendez(szamok, 0, szamok.Count-1);            
            DateTime stopTime = DateTime.Now;
            //Console.WriteLine("stopTime = {0}", stopTime);
            ListaKiir(szamok);              
            Console.WriteLine();            
            TimeSpan duration = stopTime - startTime;
            Console.WriteLine($"Eltelt idő = {duration.TotalMilliseconds}ms");                      
        }
        
        static void ListaKiir(List<int> lista_kiir)
        {
            foreach (int elem in lista_kiir)
            {
                Console.Write("{0}, ", elem);
            }
            Console.WriteLine();
        }

        public static void Gyorsrendez(List<int> lista_rendez) => Gyorsrendez(lista_rendez, 0, lista_rendez.Count - 1);
        static void Gyorsrendez(List<int> lista_rendez, int eleje, int vege)
        {
            if (eleje < vege)
            {
                int kozepe = Feloszt(lista_rendez, eleje, vege);
                Gyorsrendez(lista_rendez, eleje, kozepe - 1);
                Gyorsrendez(lista_rendez, kozepe + 1, vege);
            }
        }

        static int Feloszt(List<int> lista_feloszt, int eleje, int vege)
        {
            int kozepe = lista_feloszt[vege];
            int kozepindex = eleje;
            for (int i = eleje; i < vege; i++)
                {
                    if (lista_feloszt[i] <= kozepe)
                    {
                        int temp = lista_feloszt[i];
                        lista_feloszt[i] = lista_feloszt[kozepindex];
                        lista_feloszt[kozepindex] = temp;
                        kozepindex++;
                    }
                }

                int kozepindexErteke = lista_feloszt[kozepindex];
                lista_feloszt[kozepindex] = lista_feloszt[vege];
                lista_feloszt[vege] = kozepindexErteke;
                return kozepindex;
        }
    }
}
