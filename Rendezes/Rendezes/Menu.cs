﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace Rendezes
{
    static class Kiterjesztések
    {
        public static void Ki(this List<int> collection)
        {
            Console.Write("[ ");
            foreach (var item in collection)
            {
                Console.Write(item);
                Console.Write(" ");
            }
            Console.WriteLine("]\n");
        }
    }

    /// <summary>
    /// Menedzser: Keretprogram készítése (menü), 
    /// a feladat dokumentálása, összeszerkesztése.
    /// </summary>
    internal static class Menu
    {
        /// <summary>
        /// ez a menü "entry point"-ja.
        /// </summary>
        public static void Indít()
        {
            bool vége;
            do
            {
                vége = Menu.Kör();
                Console.WriteLine($"A {(vége?"befejezéshez":"folytatáshoz")} nyomjon meg egy gombot!");
                Console.ReadKey();
            } while (!vége);
        }

        static string menüüzenet = @"
Válasszon egyet az alábbi lehetőségek közül! A választás számát üsse be a billentyűzeten!

    (1) Gyorsrendezés fájlból beolvasott adatokkal (Számok sortöréssel elválasztva) (Rita).
    (2) Gyorsrendezés konzolról beolvasott adatokkal (Rita).
    (3) KupacRendezés fájlból beolvasott adatokkal (Ó. Attila).
    (4) KupacRendezés konzolról beolvasott adatokkal (Ó. Attila)
    (0) Kilépés a programból.";

        static bool TryReadAllLines(string path, out string[] lista)
        {
            try
            {
                lista = File.ReadAllLines(path);
                return true;
            }
            catch (Exception)
            {
                lista = null;
                return false;
            }
        }
        static List<int> Számsorozat_Fájlból()
        {
            string[] tomb;
            bool siker = false;
            Console.WriteLine("Adjon meg a sortöréssel elválasztott számokat tartalmazó fájl nevét!"); 
            do
            {
                siker = TryReadAllLines(Console.ReadLine(), out tomb);
                if (!siker)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("A fájl elérési útvonala nem található! Adjon meg új, helyes elérési útvonalat!");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            } while (!siker);
            return tomb.Select(int.Parse).ToList();
        }

        static int Szám_bekérése()
        {
            int res;
            bool sikerült = false;
            do
            {
                sikerült = int.TryParse(Console.ReadLine(), out res);
                if (!sikerült)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Ez a karaktersorozat nem határoz meg számot! Próbálja újra!");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            while (!sikerült);
            return res;
        }

        static List<int> Számsorozat_Konzolról()
        {
            Console.WriteLine("Milyen hosszú lesz a számsorozat? Adja meg a hosszát!");
            int N = Szám_bekérése();
            int[] tömb = new int[N];
            Console.WriteLine("Most adja meg a számokat egyesével enterrel elválasztva!");
            for (int i = 0; i < N; i++)
            {
                tömb[i] = Szám_bekérése();
            }
            return tömb.ToList();
        }

        static void Megmutat_és_rendez(List<int> lista, Action<List<int>> rendezés)
        {
            Console.WriteLine("Rendezés előtt:");
            lista.Ki();
            Console.WriteLine("Rendezés után:");
            rendezés(lista);
            lista.Ki();
        }
        /// <summary>
        /// ezt történik egy körben -- ez ismétlődik újra és újra...
        /// </summary>
        /// <returns>igaz, ha vége a körnek, hamis egyébként</returns>
        static bool Kör()
        {
            Console.Clear();
            Console.WriteLine(menüüzenet);
            switch (Választás_bekérése("01234"))
            {
                case '1':
                    Console.WriteLine("Gyorsrendezés fájlból beolvasott adatokkal (Rita):");
                    // Megmutat_és_rendez(Számsorozat_Fájlból(), GyorsRendezes.Gyorsrendez);
                    GyorsRendezes.Futtat(Számsorozat_Fájlból());
                    return false;
                case '2':
                    Console.WriteLine("Gyorsrendezés konzolról beolvasott adatokkal (Rita):");
                    // Megmutat_és_rendez(Számsorozat_Konzolról(), GyorsRendezes.Gyorsrendez);
                    GyorsRendezes.Futtat(Számsorozat_Konzolról());
                    return false;
                case '3':
                    Console.WriteLine("KupacRendezés fájlból beolvasott adatokkal (Ó. Attila) - under construction");
                    return false;
                case '4':
                    Console.WriteLine("KupacRendezés konzolról beolvasott adatokkal (Ó. Attila) - under construction");
                    return false;
                case '0':
                default: return true; // A fentiek miatt ez csak a 0 lehet, ezt ezzel fall-through-val jelöltem.
            }
        }


        /// <summary>
        /// Bekér egy billentyűlenyomást, ami ha nem a stringben megadott opciók egyike, akkor addig kér be újra, míg értelmes dolgot nem válaszolnak.
        /// </summary>
        /// <returns>felhasználó által beadott karakter.</returns>
        static char Választás_bekérése(string opciók)
        {
            bool sikeres_a_beolvasás;
            char c;
            do
            {
                Console.Write(">> ");
                Console.ForegroundColor = ConsoleColor.Green;
                c = Console.ReadKey().KeyChar; 
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine();

                sikeres_a_beolvasás = Tartalmazza(opciók, c);
                if (!sikeres_a_beolvasás)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Nem választható betűt nyomott meg. Kérjük, próbálkozzon újra!");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            } while (!sikeres_a_beolvasás);
            return c;
        }
        
        /// <summary>
        /// Tudom, hogy van string-hez is Contains, de bosszantott, hogy a karaktert ehhez előtte stringgé kell átváltani.
        /// </summary>
        /// <param name="s">amiben keresünk</param>
        /// <param name="c">ami karaktert keresünk</param>
        /// <returns>igaz, ha tartalmazza, egyébként hamis.</returns>
        private static bool Tartalmazza(string s, char c)
        {
            int i = 0;
            while (i < s.Length && !(s[i] == c)) { i++; }
            return i < s.Length;
        }
    }
}